///request
var request = null;
try {
    request = new XMLHttpRequest();
} catch (trymicrosoft) {
    try {
        request = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (othermicrosoft) {
        try {
            request = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (failed) {
            request = null;
        }
    }
}
if (request === null) {
    alert("Error creating request object!");
}

//utilis
function createRequest() {
    var request = null;
    try {
        request = new XMLHttpRequest();
    } catch (trymicrosoft) {
        try {
            request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (othermicrosoft) {
            try {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (failed) {
                request = null;
            }
        }
    }
    if (request === null) {
        alert("Error creating request object!");
    } else {
        return request;
    }
}

var request1 = createRequest();
var request2 = createRequest();
var requestAlt = createRequest();
var requestLogar = createRequest();

function getPageSize() {
    var xScroll, yScroll;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }
    var windowWidth, windowHeight;
    if (self.innerHeight) { // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }
    // for small pages with total height less then height of the viewport
    if (yScroll < windowHeight) {
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }
    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }
    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;
}

var fu;
function alerta(txt, f) {
    var alertText = document.getElementById("alertText");

    if (txt !== "") {

        if (txt === "connection_failure") {
            alertText.innerHTML = "Oops! Algo de errado ocorreu! Verifique sua conexão com a internet e tente novamente!";
        } else {
            alertText.innerHTML = txt;
        }
        $('.modal').modal('hide');
        $('#sysAlert').modal('show');

        if (f === "reload") {
            getEl("alertBtn").onclick = function() {
                alerta('', 'reload');
            };
        }

    } else {
        alertText.innerHTML = "";
        $('#sysAlert').modal('hide');
        if (f === "reload") {
            window.location.reload();
        }
    }

}

function actionStatus(txt, status) {
    if (!txt) {
        $('.actionStatus').hide();
        $('.actionStatus').html('');
    } else {
        $('.actionStatus').html(txt);
        $('.actionStatus').show();

        var className = "alert actionStatus";

        if (status === 'error' || status === 'success' || status === 'info') {
            var className = className + " alert-" + status;
        }
        $('.actionStatus').attr('class', className);
        $('.actionStatus').unbind('click');
        setTimeout(function() {
            $('.actionStatus').fadeOut('fast');
        }, 3600);
    }
}

function alertCo(txt) {
    var alerta = document.getElementById("alert");
    var over = document.getElementById('overlay');
    var centerOver = document.getElementById('center_overlay');
    if (txt !== "") {
        replaceText(alerta, txt);
        alerta.style.display = "block";
        over.style.display = "block";
        centerOver.style.display = "block";
        fu = f;
        pageSize = getPageSize();
        document.getElementById("overlay").style.height = pageSize[1] + "px";
    } else {
        alerta.style.display = "none";
        over.style.display = "none";
        clearText(alerta);
        document.getElementById('sysBody').style.overflow = 'auto';
        if (fu === "reload") {
            window.location.reload();
        }
    }
}
function overLoader(a) {
    var msgLoader = document.getElementById("msgLoader");
    if (a !== "") {
        $('.modal').modal('hide');
        $('#sysLoader').modal('show');
        replaceText(msgLoader, a);
    } else {
        $('#sysLoader').modal('hide');
        clearText(msgLoader);
    }

}
function clearOver(a) {
    var iframes = document.getElementsByTagName('iframe');
    for (var i = 0; i < iframes.length; i++) {
        if (a) {
            iframes[i].style.display = "none";
        } else {
            if (iframes[i].id !== 'upload_target') {
                iframes[i].style.display = "block";
            }
        }
    }
}
function alertIn(el, text) {
    el.innerHTML += '<div class="alert fade in" id="alertMembers"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> ' + text + ' </div>';
}
function replaceText(el, text) {
    if (el !== null) {
        clearText(el);
        var newNode = document.createTextNode(text);
        el.appendChild(newNode);
    }
}
function clearText(el) {
    if (el !== null) {
        if (el.childNodes) {
            for (var i = 0; i < el.childNodes.length; i++) {
                var childNode = el.childNodes[i];
                el.removeChild(childNode);
            }
        }
    }
}
function getText(el) {
    var text = "";
    if (el !== null) {
        if (el.childNodes) {
            for (var i = 0; i < el.childNodes.length; i++) {
                var childNode = el.childNodes[i];
                if (childNode.nodeValue !== null) {
                    text = text + childNode.nodeValue;
                }
            }
        }
    }
    return text;
}
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++)
    {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x === c_name)
        {
            return unescape(y);
        }
    }
}
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function checkCookie() {
    var username = getCookie("username");
    if (username !== null && username !== "") {
        alert("Welcome again " + username);
    } else {
        username = prompt("Please enter your name:", "");
        if (username !== null && username !== "") {
            setCookie("username", username, 365);
        }
    }
}
function arrumar(value, metodo) {
    var string;
    if (metodo === "send") {
        string = value.join("|");
    } else {
        string = value.split("|");
    }
    return string;
}

function display(id) {
    el = document.getElementById(id);
    if (el.style.display === "block" || el.style.display !== "none") {
        el.style.display = "none";
    } else {
        el.style.display = "block";
    }
    return el.style.display;
}
function getEl(id) {
    el = document.getElementById(id);
    return el;
}
function creatOption(txt, id, value) {
    var el = document.getElementById(id);
    var option = document.createElement("option");

    option.text = txt;
    option.value = value;
    el.add(option, 1);
}
function clearOption(el) {
    if (el !== null) {
        if (el.childNodes) {
            for (var i = 0; i < el.childNodes.length; i++) {
                var childNode = el.childNodes[i];
                el.removeChild(childNode);
            }
        }
        var a;
        for (a = el.length - 1; a >= 0; a--) {
            if (el.options[a].selected) {
                el.remove(a);
            }
        }
    }
}
var arrayDayAbb = new Array("Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab");
var arrayMonth = new Array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
function loadBirthday() {

}
function loadTimeFields(el) {
    for (var a = 1; a <= 24; a++) {
        var val = a;
        if (a < 10) {
            val = '0' + a;
        }
        $(el + ' .hoursField').append(new Option(val, val));
    }
    for (var a = 1; a <= 60; a++) {
        var val = a;
        if (a < 10) {
            val = '0' + a;
        }
        $(el + ' .minField').append(new Option(val, val));
    }
}
function loadDateFields(elDate) {
    for (var d = 1; d <= 31; d++) {
        var day = d;

        if (d < 10) {
            day = "0" + d;
        }
        $(elDate + ' .dayField').append(new Option(day, day));
    }
    for (var m = 1; m <= 12; m++) {
        var nMonth = m;
        var month = arrayMonth[m - 1];
        if (m < 10) {
            nMonth = "0" + m;
        }
        $(elDate + ' .monthField').append(new Option(month, nMonth));
    }
    var year = parseInt(sysDate[0]) - 1;
    for (var y = 0; y <= 100; y++) {
        year = year + 1;
        $(elDate + ' .yearField').append(new Option(year, year));
    }
}

function returnDateTime(elDate, elTime) {
    var day = $(elDate + ' .dayField option:selected').val();
    var month = $(elDate + ' .monthField option:selected').val();
    var year = $(elDate + ' .yearField option:selected').val();
    var hours = $(elTime + ' .hoursField option:selected').val();
    var min = $(elTime + ' .minField option:selected').val();
    return year + '-' + month + '-' + day + ' ' + hours + ':' + min;
}
function creatDateTimeArray(dateTime){
    var date = dateTime.substr(0, 10);
    var dateArray = date.split("-");
    var time = dateTime.substr(11,15);
    var timeArray = time.split(":");
    var dateTimeArray = new Array(dateArray[0], dateArray[1], dateArray[2], timeArray[0], timeArray[1]);
    return dateTimeArray;
} 
function returnDate(elDate) {
    var day = $(elDate + ' .dayField option:selected').val();
    var month = $(elDate + ' .monthField option:selected').val();
    var year = $(elDate + ' .yearField option:selected').val();
    var rtn = year + "-" + month + "-" + day;
    return rtn;
}
function youtubeId(url) {

    var youtube_id;
    youtube_id = url.replace(/^[^v]+v.(.{11}).*/, "$1");
    return youtube_id;

}
function validMail(mail) {
    var rtn = true;

    if (mail === "" || mail.indexOf('@') === -1 || mail.indexOf('.') === -1) {
        rtn = false;
    } else {
        rtn = true;
    }

    return rtn;
}

function red(el) {
    el.style.backgroundColor = "#FF4646";
    el.style.color = "#FFF";
}

function green(el) {
    el.style.backgroundColor = "#B3FF99";
    el.style.color = "#000";
}

function white(el) {
    el.style.backgroundColor = "#FFF";
    el.style.color = "#000";
}

function SomenteNumero(e) {
    var tecla = (window.event) ? event.keyCode : e.which;
    if ((tecla > 47 && tecla < 58))
        return true;
    else {
        if (tecla === 8 || tecla === 0)
            return true;
        else
            return false;
    }
}
function clear(el) {

    var clear = document.createElement("div");
    clear.className = "clear";
    el.appendChild(clear);
}

//////////////// UPLOAD ////////////////

function startUpload() {
    overLoader('Fazendo upload...');
    return true;
}
//// btns /////
function newBtn(el, type, title, f) {
    var btn = document.createElement("a");
    btn.className = type;
    btn.title = title;
    btn.onclick = f;
    btn.href = "#";
    el.appendChild(btn);
}

////////// others /////////
function teste() {
    alert('This is working!');
    return false;
}
function sysScroll(a) {
    $("html, body").animate({scrollTop: a}, "slow");
}
function emptyTab(el) {
    var emptyEl = document.createElement("div");
    emptyEl.className = "emptyTab textCenter";
    replaceText(emptyEl, "Nenhum conteúdo adicionado!");
    el.appendChild(emptyEl);
}
function keepLink(id, urlTitle) {
    var request = createRequest();

    var url = "/sys/service_us.php?fun=keepLink&id=" + id;
    request.open("GET", url, true);
    request.onreadystatechange = function() {
        confirm();
    };
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRequestHeader("charset", "utf-8");
    request.send(null);

    function confirm() {
        if (request.readyState === 4) {
            if (request.status === 200) {
                window.history.pushState('Object', "", urlTitle);
            } else {
                if (request.status === 400) {
                    var alert_txt = request.responseText;
                    alerta(alert_txt);
                } else {
                    alerta("connection_failure");
                }
            }
        }
    }
}

function clearLink() {
    window.history.pushState('Object', "", "/" + pg);
}

function iniTinymce(elements) {
    tinymce.init({
        selector: "textarea",
        mode: "exact",
        elements: elements,
        plugins: ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        autosave_ask_before_unload: false,
        max_height: 200,
        min_height: 160,
        height: 180
    });
}


function valData(data) {//dd/mm/aaaa

    day = data.substring(0, 2);
    month = data.substring(3, 5);
    year = data.substring(6, 10);

    if ((month === 01) || (month === 03) || (month === 05) || (month === 07) || (month === 08) || (month === 10) || (month === 12)) {//mes com 31 dias
        if ((day < 01) || (day > 31)) {
            alert('invalid date');
        }
    } else

    if ((month === 04) || (month === 06) || (month === 09) || (month === 11)) {//mes com 30 dias
        if ((day < 01) || (day > 30)) {
            alert('invalid date');
        }
    } else

    if ((month === 02)) {//February and leap year
        if ((year % 4 === 0) && ((year % 100 !== 0) || (year % 400 === 0))) {
            if ((day < 01) || (day > 29)) {
                alert('invalid date');
            }
        } else {
            if ((day < 01) || (day > 28)) {
                alert('invalid date');
            }
        }
    }
}
function strToBool(str){
    var falseArray = new Array('false', '-1', '0', 'null', 'none', '', ' ');
    var bool = true;
    if($.inArray(str, falseArray) > 0)
        bool = false;
    return bool;
}