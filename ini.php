<?php
session_start();
ob_start();
date_default_timezone_set('America/Sao_Paulo');

//*************************************************
/// define o e-mail a ser enviado os contatos ////
$contact_mail = "ricardoapvieira17@gmail.com";
/* Define o e-mail e senha de uma usuário do Gmail
 * que servira como servidor de envio SMTP SSL/TLS
 */
$user_name = "no-reply-tcc4tia@abelhanetwork.com"; //Usuario
$user_pass = "thAspuTa8"; ///Senha
//***************************************************

$id = $_REQUEST['id'];
$fun = $_REQUEST['fun'];
$pg = $_REQUEST['pg'];
$ip = getenv('REMOTE_ADDR');
$FriendlyUrl = $_GET['FriendlyUrl'];
$urlServer = $_SERVER['SERVER_NAME'];
$urlSource = $_SERVER ['REQUEST_URI'];
$url = $urlServer . $urlSource;
$urlArray = explode("/", substr($urlSource, 1));

///////////////// Set UTF-8 //////////////////////
setlocale(LC_ALL, 'pt_BR.utf8');
header('Content-Type: text/html; charset=utf-8');

///Algumas funções de segurança e retorno AJAX//
function filterHtml($a){
    $a = strip_tags($a);
    $a = addslashes($a);
    $a = htmlspecialchars($a);
    return $a;
}
function anti_injection($a){
    $a = trim($a);//limpa espaços vazio
    $a = strip_tags($a);//tira tags html e php
    $a = addslashes($a);//Adiciona barras invertidas a uma string
    return $a;
}
function filterRequest($a){
    $a = filterHtml($a);
    $a = anti_injection($a);
    return $a;
}

function printErro($msg) {
    http_response_code(400);
    echo $msg;
    exit();
}
//////// Chama php mailer e funções adcionais ////
require "phpmailer/class.phpmailer.php";

//sistema inicializado
//Chama a função de envio configurado com PHP mailer + Gmail
require "send.php";