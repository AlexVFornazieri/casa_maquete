<div class="container" id="contacts">
    <header><h1>Contatos</h1></header>
    <div class="span11 form">
    <h4>Deixe um recado:</h4>    
    <form id="contactForm">
            <div class="span6 form-horizontal">
                <div class="component"><!-- Nome -->
                    <div class="control-group" id="groupName">
                        <label class="control-label" for="inputName">Nome:</label>
                        <div class="controls">
                            <input id="inputName" name="inputName" type="text" placeholder="Nome" class="input-xlarge">
                        </div>
                    </div>
                </div>
                <div class="component"><!-- Sobrenome -->
                    <div class="control-group" id="groupLastname">
                        <label class="control-label" for="inputLastname">Sobrenome:</label>
                        <div class="controls">
                            <input id="inputLastname" name="inputLastname" type="text" placeholder="Sobrenome" class="input-xlarge">
                        </div>
                    </div>
                </div>
                <div class="component"><!-- E-mail -->
                    <div class="control-group" id="groupMail">
                        <label class="control-label" for="inputMail">E-mail:</label>
                        <div class="controls">
                            <input id="inputMail" name="inputMail" type="text" placeholder="exemplo@aln.com" class="input-xlarge">
                        </div>
                    </div>
                </div>
                <div class="component"><!-- Telefone -->
                    <div class="control-group" id="groupFone">
                        <label class="control-label" for="inputFone">Fone:</label>
                        <div class="controls">
                            <input id="inputFone" name="inputFone" onkeypress="return SomenteNumero(event);" type="text" placeholder="DD + Número; Ex.: 4412345678" class="input-xlarge">
                        </div>
                    </div>
                </div>
                <div class="component"><!-- Cidade-UF -->
                    <div class="control-group" id="groupCity">
                        <label class="control-label" for="inputCity">Cidade-UF:</label>
                        <div class="controls">
                            <input id="inputCity" name="inputCity" type="text" placeholder="Ex.: Paranavaí-PR" class="input-xlarge">
                        </div>
                    </div>
                </div>
            </div>
            <div class="component"><!-- Texto -->
                <div class="control-group" id="groupText">
                    <textarea id="inputText" class="span5" rows="9"></textarea>
                </div>
            </div>
            <div id="alertIn"></div>
            <a class="btn btn-primary pull-right" href="#" onclick="sendContact();">Enviar</a>
            <div class="clear"></div>
        </form>
    </div>
</div>