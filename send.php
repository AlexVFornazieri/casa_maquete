<?php

function sendMail($name, $assunto, $to, $from, $msg) {
    global $user_name;
    global $user_pass;
// Abaixo começaremos a utilizar o PHPMailer. 

    /*
      Aqui criamos uma nova instância da classe como $mail.
      Todas as características, funções e métodos da classe
      poderão ser acessados através da variável (objeto) $mail.
     */
    $mail = new PHPMailer(); // 
// Define o método de envio
    $mail->Mailer = "smtp";

// Define que a mensagem poderá ter formatação HTML
    $mail->IsHTML(true); //
// Define que a codificação do conteúdo da mensagem será utf-8
    $mail->CharSet = "utf-8";

// Define que os emails enviadas utilizarão SMTP Seguro tls
    $mail->SMTPSecure = "tls";

// Define que o Host que enviará a mensagem é o Gmail
    $mail->Host = "smtp.gmail.com";

//Define a porta utilizada pelo Gmail para o envio autenticado
    $mail->Port = "587";

// Deine que a mensagem utiliza método de envio autenticado
    $mail->SMTPAuth = "true";

// Define o usuário do gmail autenticado responsável pelo envio
    $mail->Username = $user_name;

// Define a senha deste usuário citado acima
    $mail->Password = $user_pass;

// Defina o email e o nome que aparecerá como remetente no cabeçalho
    $mail->From = $from;
    $mail->FromName = $name;

// Define o destinatário que receberá a mensagem
    $mail->AddAddress($to);

    /*
      Define o email que receberá resposta desta
      mensagem, quando o destinatário responder
     */
    $mail->AddReplyTo($from, $mail->FromName);

// Assunto da mensagem
    $mail->Subject = $assunto;

// Toda a estrutura HTML e corpo da mensagem
    $mail->Body = $msg;

// Controle de erro ou sucesso no envio
    if (!$mail->Send()) {

        return $mail->ErrorInfo;
    } else {

        return true;
    }
}