<header id="header">
    <div class="navbar">
        <div class="navbar-inner ">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="#">A·L·N.com</a>
                <div class="nav-collapse collapse navbar-responsive-collapse">
                    <ul class="nav">
                        <?php
                        if ($user) {
                            ?>
                        <li <?php if ($pg == "home") { ?> class="active" <?php } ?>><a href="?pg=controls">Controles</a></li>
                        <?php
                        }else{
                        ?>
                        <li <?php if ($pg == "home") { ?> class="active" <?php } ?>><a href="?pg=home">Home</a></li>
                        <?php } ?>
                        <li <?php if ($pg == "contatos") { ?> class="active" <?php } ?>><a href="?pg=contatos">Contatos</a></li>
                    </ul>
                    <ul class="nav pull-right">

                        <li class="dropdown">
                            <a><b class="icon-user"></b> <?php echo $user_name; ?></a>
                            <?php if ($user) { ?>
                            <li class="divider-vertical"></li>
                            <li><a href="logout.php">Sair</a></li>
                        <?php } ?>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->
</header>