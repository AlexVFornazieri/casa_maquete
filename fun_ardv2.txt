#include <SPI.h>
#include <Ethernet.h>

//Configura��es do Ethernet Shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//Essa parte eu sei que � preciso modificar de acordo com a minha rede..
byte ip[] = { 192,168,0, 103}; // ip que o arduino assumir�
byte gateway[] = { 192,168,0, 1 }; // ip do roteador
byte subnet[] = { 255, 255, 255, 0 };

// String que representa o estado dos dispositivos
char Luz[11] = "00000000L#";

EthernetServer server(8081); // Cria o servidor na porta 8081

// String onde � guardada as msgs recebidas
char msg[11] = "00000000L#";

void setup() {
Ethernet.begin(mac, ip, gateway, subnet);
	server.begin();
	pinMode(2,OUTPUT);
	pinMode(3,OUTPUT);
	pinMode(4,OUTPUT);
	pinMode(5,OUTPUT);
        pinMode(6,OUTPUT);
        pinMode(7,OUTPUT);
	pinMode(8,OUTPUT);
        pinMode(9,OUTPUT);
}

void loop() {
EthernetClient client = server.available();
// SE receber um caracter...
if (client) {
// guarda o caracter na string 'msg'
msg[1]=msg[2]; msg[2]=msg[3]; msg[3]=msg[4]; msg[4]=msg[5]; msg[5]=msg[6]; msg[6]=msg[7]; msg[7]=msg[8]; msg[8]=msg[9];
msg[9]=msg[10]; msg[10] = client.read();

if (msg[10]=='#') {
switch(msg[9]) {
case 'R':
// Se receber o comando 'R#' envia de volta o status dos
// dispositivos. (Que � a string 'Luz')
client.write(Luz);
break;
case 'L':
// Caso L#, ele copia os 4 bytes anteriores p/ a
// string 'Luz' e cada byte representa um
// dispositivo, onde '1'=ON e '0'=OFF
Luz[0]=msg[1];
Luz[1]=msg[2];
Luz[2]=msg[3];
Luz[3]=msg[4];
Luz[4]=msg[5];
Luz[5]=msg[6];
Luz[6]=msg[7];
Luz[7]=msg[8];
if (Luz[0]=='1') digitalWrite(2,HIGH); else digitalWrite(2,LOW);
if (Luz[1]=='1') digitalWrite(3,HIGH); else digitalWrite(3,LOW);
if (Luz[2]=='1') digitalWrite(4,HIGH); else digitalWrite(4,LOW);
if (Luz[3]=='1') digitalWrite(5,HIGH); else digitalWrite(5,LOW);
if (Luz[4]=='1') digitalWrite(6,HIGH); else digitalWrite(6,LOW);
if (Luz[5]=='1') digitalWrite(7,HIGH); else digitalWrite(7,LOW);
if (Luz[6]=='1') digitalWrite(8,HIGH); else digitalWrite(8,LOW);
if (Luz[7]=='1') digitalWrite(9,HIGH); else digitalWrite(9,LOW);
break;

}
}
}
}