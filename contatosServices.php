<?php
require 'ini.php';

function sendContact($name, $lastname, $mail, $to, $city, $fone, $text){
    
    $name = $name . " " . $lastname;
    
    $from = $mail;
    
    $assunto = "Contato via WEB";
    
$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Contato via WEB!</title>
<style type="text/css">
body {
    padding-left: 15px;
    padding-top: 15px;
    width: 600px;
    color: black;
    font-size: 16px;
    font-family: arial, sans-serif;
}
#header{
    background-color: #AAA;
    border: #000 solid 1px;
    height: 30px;
    width: 500px;
    line-height: 30px;
    text-align: center;
    font-weight: bold;
    font-size: 20px;
    color: black;
}#footer{
    background-color: #AAA;
    border: #000 solid 1px;
    height: 30px;
    width: 500px;
    line-height: 30px;
    text-align: center;
    font-size: 16px;
    color: black;
    margin-top: 15px;
}body span{
    font-weight: bold;
}
</style>
</head>

<body>
    <div id="header">Contato via WEB!</div>
    <p><span>Nome: </span>' . $name . '.</p>
    <p><span>E-mail: </span>' . $mail . '.</p>
    <p><span>Cidade-UF: </span>' . $city . '</p>
    <p><span>Tel.(1): </span>' . $fone. '</p>
    <p><span>Mensagem: </span></p>
    <div>' . $text . '</div>

    <div id="footer"></div>
</body>
</html>';    
    
    $send = sendMail($name, $assunto, $to, $from, $html);
    if($send){
        return true;
    }else{
        return $send;
    }  
}


if ($fun == "sendContact") {

    $name = filterRequest($_REQUEST['name']);
    $lastname = filterRequest($_REQUEST['lastname']);
    $mail = filterRequest($_REQUEST['mail']);
    $fone = filterRequest($_REQUEST['fone']);
    $city = filterRequest($_REQUEST['city']);
    $text = filterRequest($_REQUEST['text']);

    $rtn = false;

    $erros = 0;

    if ($name == "") {
        $erros = $erros + 1;
    }
    if ($lastname == "") {
        $erros = $erros + 1;
    }
    if ($mail == "") {
        $erros = $erros + 1;
    }if ($city == "") {
        $erros = $erros + 1;
    }
    if ($text == "") {
        $erros = $erros + 1;
    }

    if ($erros <= 0) {
        $rtn = true;
    } else {
        printErro('Oops! Preecha o formulário corretamente! '.$name.', '. $lastname.', '. $fone.', '.$mail.', '.$city.', '.$text);
    }

    if ($rtn) {

        $to = $contact_mail;

        $send = sendContact($name, $lastname, $mail, $to, $city, $fone, $text);

        if ($send) {
            print 'Contato enviado com sucesso!';
        } else {
            printErro('Oops! Falha no envio! Se preferir pode enviar um E-mail diretamente para <b>' . $contact_mail . '</b>' . $send);
        }
    }
}