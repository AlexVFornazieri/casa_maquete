<?php
$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
// Se conecta ao IP e Porta:
socket_connect($sock, "192.168.0.103", 8081);  // Não sei como adpatar esse IP (internet,??)
// Executa a ação correspondente ao botão apertado.
if (isset($_POST['bits'])) {
    $msg = $_POST['bits'];

    if (isset($_POST['ligaTodos'])) {
        $msg[0] = '0';
        $msg[1] = '0';
        $msg[2] = '0';
        $msg[3] = '0';
        $msg[4] = '0';
        $msg[5] = '0';
        $msg[6] = '0';
        $msg[7] = '0';
    }

    if (isset($_POST['desligaTodos'])) {
        $msg[0] = '1';
        $msg[1] = '1';
        $msg[2] = '1';
        $msg[3] = '1';
        $msg[4] = '1';
        $msg[5] = '1';
        $msg[6] = '1';
        $msg[7] = '1';
    }

    if (isset($_POST['btn01'])) {
        if ($msg[0] == '0') {
            $msg[0] = '1';
        } else {
            $msg[0] = '0';
        }
    }
    if (isset($_POST['btn02'])) {
        if ($msg[1] == '0') {
            $msg[1] = '1';
        } else {
            $msg[1] = '0';
        }
    }
    if (isset($_POST['btn03'])) {
        if ($msg[2] == '0') {
            $msg[2] = '1';
        } else {
            $msg[2] = '0';
        }
    }
    if (isset($_POST['btn04'])) {
        if ($msg[3] == '0') {
            $msg[3] = '1';
        } else {
            $msg[3] = '0';
        }
    }
    if (isset($_POST['btn05'])) {
        if ($msg[4] == '0') {
            $msg[4] = '1';
        } else {
            $msg[4] = '0';
        }
    }
    if (isset($_POST['btn06'])) {
        if ($msg[5] == '0') {
            $msg[5] = '1';
        } else {
            $msg[5] = '0';
        }
    }
    if (isset($_POST['btn07'])) {
        if ($msg[6] == '0') {
            $msg[6] = '1';
        } else {
            $msg[6] = '0';
        }
    }
    if (isset($_POST['btn08'])) {
        if ($msg[7] == '0') {
            $msg[7] = '1';
        } else {
            $msg[7] = '0';
        }
    }
    socket_write($sock, $msg, strlen($msg));
}

socket_write($sock, 'R#', 2); //Requisita o status do sistema.
// Espera e lê o status e define a cor dos botões de acordo.
$status = socket_read($sock, 10);
if (($status[8] == 'L') && ($status[9] == '#')) {
    if ($status[0] == '1') {
        $cor1 = '';
    } else {
        $cor1 = 'btn-success';
    }
    if ($status[1] == '1') {
        $cor2 = '';
    } else {
        $cor2 = 'btn-success';
    }
    if ($status[2] == '1') {
        $cor3 = '';
    } else {
        $cor3 = 'btn-success';
    }
    if ($status[3] == '1') {
        $cor4 = '';
    } else {
        $cor4 = 'btn-success';
    }
    if ($status[4] == '1') {
        $cor5 = '';
    } else {
        $cor5 = 'btn-success';
    }
    if ($status[5] == '1') {
        $cor6 = '';
    } else {
        $cor6 = 'btn-success';
    }
    if ($status[6] == '1') {
        $cor7 = '';
    } else {
        $cor7 = 'btn-success';
    }
    if ($status[7] == '1') {
        $cor8 = '';
    } else {
        $cor8 = 'btn-success';
    }
    ?>
    <section class="container">
        <div class="page-header">
            <h1>Painel de Controle</h1>
        </div>

        <form method="post" action="index.php?pg=controls">
            <div class="well lampadas">
                <div class="page-header">
                    <h3>Lampadas</h3>
                </div>
                <input type="hidden" name="bits" value="<?php echo $status ?>">
                
                <p><button class="btn btn-large btn-block <?php echo $cor2 ?>" type="Submit" Name="btn02">Sala</button></p>
                <p><button class="btn btn-large btn-block <?php echo $cor3 ?>" type="Submit" Name="btn03">Quarto 1</button></p>
                <p><button class="btn btn-large btn-block <?php echo $cor4 ?>" type="Submit" Name="btn04">Cozinha</button></p>
                <p><button class="btn btn-large btn-block <?php echo $cor5 ?>" type="Submit" Name="btn05">Quarto Casal</button></p>
                <p><button class="btn btn-large btn-block <?php echo $cor6 ?>" type="Submit" Name="btn06">Banheiro</button></p>
                <p><button class="btn btn-large btn-block <?php echo $cor7 ?>" type="Submit" Name="btn07">Fora</button></p>
                <p><button class="btn btn-large btn-block <?php echo $cor8 ?>" type="Submit" Name="btn08">Quarto2</button></p>
                <p><button class="btn btn-large btn-block btn-danger" type="Submit" Name="desligaTodos">Desliga Tudo</button></p>
                <p><button class="btn btn-large btn-block btn-primary" type="Submit" Name="ligaTodos">Liga Tudo</button></p>
            <!--<p><button class="btn btn-large btn-block <?php echo $cor1 ?>" type="Submit" Name="btn01">Rele 8</button></p>-->
            </div>
            <div class="well">
                <p style="font-size: 30px; color: #040404;"><a href="index.php?pg=controls">
                        <span class="icomoon-spinner6"></span>Atualizar
                    </a></p>
            </div>
        </form>
    </section>
    <?php
}
// Caso ele não receba o status corretamente, avisa erro.
else {
    ?>
    <div style="height: 50px"></div>
    <div class="alert alert-error alert-block">
        <h4>Falha de conexão!</h4>
        <p>Não foi possivel se comunicar com a casa! Verifique as conexões e disponibilidade de energia no sistema!</p>
        <p>
            <a class="btn btn-danger" href="index.php?pg=controls">Tentar Novamente</a> <a class="btn" href="logout.php">Sair do sistema</a>
        </p>
    </div>
    <?php
}
socket_close($sock);

/*
//Requisita temperatura do sensor

socket_write($sock, 'T#', 2); //Requisita o status do sistema.
// Espera e lê o status e define a cor dos botões de acordo.
$temp = socket_read($sock, 1);
if ($temp != '') {
    print '<h1>' . $temp . '</h1>';
} else {
    echo 'Erro ao ler a temperatura';
}
socket_close($temp);

