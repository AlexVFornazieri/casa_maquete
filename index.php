<?php
require 'verifica.php';
$pg = $_GET['pg'];
if ($pg == "") {
    $pg = 'home';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Automação residencial</title>
        <!-- Bootstrap CSS -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.css" type="text/css">
        <!-- load CSS sysLayout !--> 
        <link rel="stylesheet" href="style.css" type="text/css" />
        <!-- load CSS icoomoon !--> 
        <link rel="stylesheet" href="icomoon/style.css" type="text/css" />
        <link rel="stylesheet" href="utilities.css" type="text/css" />
        <?php if($pg == 'controls'){ ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="5; URL=index.php?pg=controls">
        <?php } ?>
        
    </head>
    <body>
        <!-- div de inserção utilities JSP !-->
        <div id="sysAlert" class="modal hide fade" tabindex="-1" aria-labelledby="myModalLabel">
            <div class="modal-body">
                <div id="alertText" class="textCenter"></div>
            </div>
            <div class="modal-footer textCenter">
                <a id="alertBtn" class="btn btn-primary" onclick="alerta('');">Ok!</a>
            </div>
        </div>
        <div id="sysLoader" class="modal hide fade" tabindex="-1" aria-labelledby="myModalLabel">
            <div class="modal-body">
                <div style='width: 32px; margin: 0 auto 0 auto'>
                    <img src="imgs/overLoader.gif">
                </div>
                <h2 id="msgLoader" class='textCenter'></h2>
            </div>
        </div>
        
        <div id="wrap">
            <?php include 'top.php'; ?>
            <!-- Begin page content -->
            <div class="container" id="conteudo">
                <?php include $pg . '.php'; ?>
            </div>

            <div id="push"></div>
        </div>

        <?php include 'footer.php'; ?>
        
        <script src="jquery/jquery-2.0.3.min.js"></script>
        <script src="jquery/jquery.maskedinput.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="utilities.js"></script>
        <script src="contatos.js"></script>
    </body>
</html>
