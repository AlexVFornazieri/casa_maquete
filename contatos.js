function sendContact() {
    var request = createRequest();
    var inputName = getEl('inputName');
    var inputLastname = getEl('inputLastname');
    var inputMail = getEl('inputMail');
    var inputCity = getEl('inputCity');
    var inputFone = getEl('inputFone');
    var inputText = getEl('inputText');

    if (checkContactForm()) {
        var url = "contatosServices.php?fun=sendContact";

        request.open("POST", url, true);
        request.onreadystatechange = sendConfirmation;
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setRequestHeader("charset", "utf-8");
        request.send("name=" + inputName.value +
                "&lastname=" + inputLastname.value +
                "&mail=" + inputMail.value +
                "&city=" + inputCity.value +
                "&fone=" + inputFone.value +
                "&text=" + inputText.value
                );

        overLoader('Aguarde...');
    }
    function sendConfirmation() {
        if (request.readyState === 4) {
            if (request.status === 200) {
                var al = request.responseText;
                alerta(al);
                getEl('contactForm').reset();
            } else {
                if (request.status === 400) {
                    alerta(request.responseText);
                } else {
                    alerta("connection_failure " + request.status);
                }
            }
        }
    }
}
function checkContactForm() {
    var rtn = false;
    var inputName = getEl('inputName');
    var inputLastname = getEl('inputLastname');
    var inputMail = getEl('inputMail');
    var inputCity = getEl('inputCity');
    var inputFone = getEl('inputFone');
    var inputText = getEl('inputText');
    $('.control-group').removeClass('error');
    var erros = 0;
    if (inputName.value.length < 1) {
        $('#groupName').addClass('error');
        if (erros === 0) {
            inputName.focus();
        }
        erros = erros + 1;
    }
    if (inputLastname.value.length < 1) {
        $('#groupLastname').addClass('error');
        if (erros === 0) {
            inputLastname.focus();
        }
        erros = erros + 1;
    }
    if (!validMail(inputMail.value)) {
        $('#groupMail').addClass('error');
        if (erros === 0) {
            inputMail.focus();
        }
        erros = erros + 1;
    }
    if (inputCity.value.length < 1) {
        $('#groupCity').addClass('error');
        if (erros === 0) {
            inputCity.focus();
        }
        erros = erros + 1;
    }
    if (inputFone.value.length > 0 && inputFone.value.length < 10) {
        $('#groupFone').addClass('error');
        if (erros === 0) {
            inputFone.focus();
        }
        erros = erros + 1;
    }
    if (inputText.value.length < 0) {
        $('#groupText').addClass('error');
        if (erros === 0) {
            inputText.focus();
        }
        erros = erros + 1;
        replaceText(getEl("alertInputTxt"), "Digite no mínimo 20 Caracteres!");
    }
    if (erros === 0) {
        rtn = true;
    }
    return rtn;
}