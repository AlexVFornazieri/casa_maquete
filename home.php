<?php
if ($user) {
    $control = header('location: index.php?pg=controls');
} else { ?>
    <section class="container">
        <div class="page-header">
            <h1>Controle tudo sem sair do lugar!</h1>
        </div>
        <div class="span7">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In venenatis arcu non dolor aliquet cursus. Aliquam magna mauris, pellentesque quis tempor suscipit, luctus vitae diam. Suspendisse faucibus elit tellus, vel pellentesque enim commodo nec. Duis aliquam eros tortor, vel porttitor arcu faucibus euismod. Proin convallis enim eu nisl ultrices, ac dictum lectus tristique. Nunc venenatis condimentum dignissim. Curabitur euismod mauris at suscipit ornare. Praesent non feugiat felis. Etiam venenatis, ipsum quis fermentum euismod, massa dolor ullamcorper erat, sed dictum leo dolor sed turpis. Phasellus efficitur leo vitae fermentum vestibulum. Pellentesque hendrerit dui in vulputate mattis. </p>
            <img src="imgs/arduindo.png" alt="Logo do Arduindo" width="200px" class="img">
        </div>
        <div class="span4">
            <form class="form-signin" name="form-login" method="POST" action="logar.php?fun=logar">
                <h2 class="form-signin-heading">Entrar no sistema</h2>
                <input type="text" name="login" class="input-block-level" placeholder="Login - E-mail">
                <input type="password" name="senha" class="input-block-level" placeholder="Senha">
                <input type="submit" class="btn btn-large btn-primary" type="submit" value="Entrar">
            </form>
        </div>
    </section>

<?php

}